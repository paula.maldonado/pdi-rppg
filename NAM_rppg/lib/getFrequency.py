import numpy as np
import time


class getFrequency(object):

    def __init__(self):
        self.data_buffer = []
        self.data_buffer_max_size = 20
        self.frameIn = np.zeros((10, 10))
        self.ROI_means = []
        self.normalizeSignal = []
        self.meanROI = 0
        self.desvROI = 0

        # time
        self.times = []
        self.initialTime = time.time()
        self.fps = 0

        # Freq
        self.magnitude = []
        self.freqs = []
        self.bpm = 0

    # Dado que estamos obteniendo los datos en vivo, utilizamos un buffer con un tamaño determinado
    # para que de esta forma sea más sencillo trabajar con los datos
    def FrequenceAnalyzer(self):
        self.times.append(time.time() - self.initialTime)
        self.frameOut = self.frameIn
        self.data_buffer.append(self.ROI_means)
        Large_Actual = len(self.data_buffer)
        if Large_Actual > self.data_buffer_max_size:
            del self.data_buffer[0]
            del self.times[0]
            Large_Actual = len(self.data_buffer)

        # Normalizamos los datos del buffer
        self.meanROI, self.desvROI = np.mean(self.data_buffer), np.std(self.data_buffer)

        self.normalizeSignal.append((self.data_buffer[-1] - self.meanROI) / self.desvROI)
        Large_Actual_Normalize = len(self.normalizeSignal)
        if Large_Actual_Normalize > self.data_buffer_max_size:
            del self.normalizeSignal[0]
            Large_Actual_Normalize = len(self.normalizeSignal)

        # Tomamos en cuenta después de los 15 primeros datos
        if Large_Actual_Normalize > 15:
            self.fps = float(Large_Actual_Normalize) / (self.times[-1] - self.times[0])

            # Interpolamos los datos normalizados
            lifetime = np.linspace(self.times[0], self.times[-1], Large_Actual_Normalize)
            interpolated = np.interp(lifetime, self.times, self.normalizeSignal)
            interpolated = np.hamming(Large_Actual_Normalize) * interpolated
            interpolated = interpolated - np.mean(interpolated)

            # Aplicamos la transformada de Fourier para obtener las frecuencias
            raw = np.fft.rfft(interpolated)
            self.magnitude = np.abs(raw)
            self.freqs = float(self.fps) / Large_Actual_Normalize * np.arange(Large_Actual_Normalize)

            freq_aux = 60. * self.freqs
            filterMaxMin = np.where((freq_aux > 50) & (freq_aux < 180))
            magnitudeFilteredByMaxMin = self.magnitude[filterMaxMin]

            self.freqs = freq_aux[filterMaxMin]
            self.magnitude = magnitudeFilteredByMaxMin
            maxValueID = np.argmax(magnitudeFilteredByMaxMin)

            self.bpm = self.freqs[maxValueID]
