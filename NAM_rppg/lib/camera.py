import cv2
import numpy as np

cameraPort = 1
class Camera(object):
    def __init__(self, camera=cameraPort):
        self.cam = cv2.VideoCapture(camera, cv2.CAP_DSHOW)

    def get_frame(self):
        ret, frame = self.cam.read()
        if not ret:
            frame = np.ones((480, 640, 3), dtype=np.uint8)
            cv2.putText(frame, "(Error: Camera is not accessible)",
                        (65, 220), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 256))
        return frame

    def release(self):
        self.cam.release()
