import cv2
import numpy as np

# Aqui se debe poner el path de la imagen de haarcascade
path = r"C:\\Users\\pbele\\Desktop\\ELO328\\rppg\\pdi-rppg\\NAM_rppg\\lib\\haarcascade_frontalface_default.xml"


class getForeheadROI(object):
    def __init__(self):
        self.face = None
        self.frameGray = np.zeros((10, 10))
        self.frameIn = np.zeros((10, 10))
        self.frameOut = np.zeros((10, 10))  # frame de salida

        self.neighborhood = 10  # vecinos, controla la cantidad de rectangulos que se crean
        self.face_cascade = cv2.CascadeClassifier(path)
        self.foreheadROI_coord = np.zeros((10, 10))

    def rectangleROI(self):
        self.frameOut = self.frameIn
        self.frameGray = cv2.cvtColor(self.frameIn, cv2.COLOR_BGR2GRAY)
        #Obtención de la ROI
        try:
            self.face = self.face_cascade.detectMultiScale(self.frameGray, 1.2, self.neighborhood)
            for (x, y, w, h) in self.face:
                x_Aux, w_Aux, y_Aux, h_Aux = x + w // 4, y + h // 25, x + w - w // 4, y + h // 4
                cv2.rectangle(self.frameOut, (x_Aux, w_Aux), (y_Aux, h_Aux), (0, 255, 0),
                              thickness=2)
                self.foreheadROI_coord = self.frameIn[w_Aux:h_Aux, x_Aux:y_Aux:]
        except:
            print("Can not get ROI")
            pass

    #Promedio de intensidades
    def intensityMeans(self):
        try:
            rectangle = self.foreheadROI_coord
            v1 = np.mean(rectangle[:, :, 0])
            v2 = np.mean(rectangle[:, :, 1])
            v3 = np.mean(rectangle[:, :, 2])
        except:
            v1 = 0
            v2 = 0
            v3 = 0
        return (v1 + v2 + v3) / 3
