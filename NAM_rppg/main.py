from lib.camera import Camera
from lib.getForeheadROI import getForeheadROI
from lib.getFrequency import getFrequency
import cv2

class GetPulseApp(object):

    def __init__(self):
        self.cameras = Camera(camera=0)
        self.w, self.h = 0, 0
        self.pressed = 0
        self.getForehead = getForeheadROI()
        self.getFrequency = getFrequency()
        self.bpm = 0


    def main_loop(self):
        frame = self.cameras.get_frame()

        self.getForehead.frameIn = frame
        self.getForehead.rectangleROI()

        self.getFrequency.frameIn = frame
        self.getFrequency.ROI_means = self.getForehead.intensityMeans()
        self.getFrequency.FrequenceAnalyzer()
        self.bpm = self.getFrequency.bpm

        text = "HR: %0.1f bpm" % (self.bpm)
        cv2.putText(self.getForehead.frameOut, text,
                    (25, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0))

        cv2.imshow("Original", self.getForehead.frameOut)
        if cv2.waitKey(10) & 255 == 27:  # salir del programa con esc
            print("Exiting")
            self.cameras.release()
            cv2.destroyAllWindows()
            exit()


if __name__ == "__main__":
    App = GetPulseApp()
    while True:
        App.main_loop()
