import cv2 as cv
import mediapipe as mp
import time
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from scipy.signal import butter, welch,lfilter,sosfilt
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import FastICA



class BCG:
    def __init__(self):
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_face_mesh = mp.solutions.face_mesh
        self.mp_drawing_style = mp.solutions.drawing_styles
        self.mp_face_mesh = self.mp_face_mesh.FaceMesh(static_image_mode=False, max_num_faces=1)
        self.draw_spec = self.mp_drawing.DrawingSpec(thickness=1, circle_radius=1, color=(0,255,0))
        
    first = True
    def findFace(self,img):
        
        #elegimos la mejor roi, responsiva a partir de los puntos entregados con facemesh
        try:
            height, width, _ = img.shape
            
            img = cv.cvtColor(cv.flip(img,1),cv.COLOR_BGR2RGB)
            self.results = self.mp_face_mesh.process(img)
    
            if (self.results.multi_face_landmarks):
                img_copy = img.copy()
                for landmark in self.results.multi_face_landmarks:
                    
                    nariz_sup = landmark.landmark[119]
                    x0 = int(nariz_sup.x*width)
                    y0 = int(nariz_sup.y*height)
                    
                    nariz_inf = landmark.landmark[273]
                    x3 = int(nariz_inf.x*width)
                    y3 = int(nariz_inf.y*height)
                    
                    frente_sup = landmark.landmark[67]
                    x = int(frente_sup.x*width)
                    y = int(frente_sup.y*height)
                    
                    frente_inf = landmark.landmark[296]
                    x2 = int(frente_inf.x*width)
                    y2 = int(frente_inf.y*height)
                    
                    cv.circle(img,(x0,y0),4,(0,255,0),1)
                    cv.circle(img,(x,y),4,(0,255,0),1)
                    cv.rectangle(img,(x,y),(x2,y2),(0,0,255),1)
                    cv.rectangle(img,(x0,y0),(x3,y3),(0,0,255),1)


            img = cv.cvtColor(img,cv.COLOR_RGB2BGR)
            img_copy = cv.cvtColor(img_copy,cv.COLOR_RGB2BGR)
            ROI_nariz = img_copy[y0:y3, x0:x3]
            ROI_frente = img_copy[y:y2, x:x2]
            ROI_nariz= cv.resize(ROI_nariz,(75,80))
            ROI_frente= cv.resize(ROI_frente,(100,50))
        
            # # Roi no responsiva a partir de un solo punto, se deja para probar 
            # # puntos de manera libre ya que tenemos anotadas las proporciones
            #         nariz = landmark.landmark[5]
            #         x0 = int(nariz.x*width)
            #         y0 = int(nariz.y*height)
                    
            #         frente = landmark.landmark[10]
            #         x = int(frente.x*width)
            #         y = int(frente.y*height)
        
            #         cv.circle(img,(x0,y0),4,(0,255,0),1)
            #         cv.circle(img,(x,y),4,(0,255,0),1)
            #         cv.rectangle(img,(x-40,y-20),(x+40,y+20),(0,0,255),1)
            #         cv.rectangle(img,(x0-35,y0-5),(x0+35,y0+40),(0,0,255),1)
                
            # img = cv.cvtColor(img,cv.COLOR_RGB2BGR)
            # img_copy = cv.cvtColor(img_copy,cv.COLOR_RGB2BGR)
            # ROI_frente = img_copy[y-20:y+20, x-40:x+40]
            # ROI_nariz = img_copy[y0:y0+50, x0-35:x0+35]


            return img, [x,y], [x0,y0], ROI_nariz,ROI_frente
        except:
            pass

def main():

    # params for LKT tracker
    lk_params = dict( winSize  = (15,15),
                      maxLevel = 3,
                      criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

    # params for ShiTomasi corner detection
    gftt_params = dict( maxCorners =50,
                           qualityLevel = 0.01,
                           minDistance = 1
                          )
    #principal components analysis
    def do_pca(data,n_components=5):
        pca = PCA(n_components=n_components)
        principalComponents = pca.fit_transform(data)
        principalDf = pd.DataFrame(data = principalComponents
             , columns = [0,1,2,3,4])
        return principalDf
    #Independent components analisys
    def do_ica(data,n_components=5):
        ica = FastICA(n_components=n_components)
        S_ = ica.fit_transform(data)
        return S_
    
    # filtro pasabandas
    def butter_bandpass(lowcut, highcut, fs, order=5):
            nyq = 0.5 * fs
            low = lowcut / nyq
            high = highcut / nyq
            sos = butter(order, [low, high], analog=False, btype='band', output='sos')
            return sos
    #filtro pasabandas
    def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
            sos = butter_bandpass(lowcut, highcut, fs, order=order)
            y = sosfilt(sos, data)
            return y

    def rescaleFrame(frame,scale=0.7):
        width = int(frame.shape[1] * scale)#shape[1] es el ancho
        heigth = int(frame.shape[0] * scale) #shape[0] es el alto
        dimensions = (width,heigth)
        return cv.resize(frame,dimensions,interpolation = cv.INTER_AREA)
    
    
    
    
    cap = cv.VideoCapture(cv.CAP_DSHOW)

    #INICIALIZACION DE VARIABLES
    df_nariz= pd.DataFrame(columns= [0,1,2,3,4,5,6,7,8,9,10,11,12])
    df_frente= pd.DataFrame(columns= [0,1,2,3,4,5,6,7,8,9,10,11,12])
    initial_time = time.time()
    pImg = cap.read()
    pTime = 0
    bcg = BCG()
    resta = 0
    segundo = 0
    count = 0
    nextPts_nariz = np.array([])
    nextPts_frente = np.array([])
    c=0
    fps_avg = 0
    flag= False
    avg_hr = 0
    is_first=True
    
   #se empieza a leer el stream
    while True:
        try:
            success, cImg = cap.read()
            # # desde aqui podemos cambiar el tamaño de la imagen entrante manualmente,
            # # como trabajo a futuro se debe estandarizar el tamaño de entrada
            if ((cImg.shape[0]>1280 or cImg.shape[1]>1280)):
                cImg = rescaleFrame(cImg,0.2)
            elif (cImg.shape[0]>720 or cImg.shape[1]>720):
                cImg = rescaleFrame(cImg)
            if(not success or cv.waitKey(3) & 0xFF == 27):
                print("video finalizado")
                break
            
            
            count = count + 1
            #se dejan pasar unos frames para inicializar el sistema de deteccion de corners
            if (count > 10 and count < 20):
                pImg, pPoint_frente, pPoint_nariz, pROI_nariz, pROI_frente = bcg.findFace(cImg)
                pROI_nariz = cv.cvtColor(pROI_nariz,cv.COLOR_BGR2GRAY)
                pROI_frente = cv.cvtColor(pROI_frente,cv.COLOR_BGR2GRAY)
            #comenzamos a obtener los datos en tiempo real para comparar
            if (count > 20):
                cImg, cPoint_frente, cPoint_nariz, ROI_nariz, ROI_frente = bcg.findFace(cImg)
                cTime = time.time()
                segundo = int(cTime - initial_time)
                #vamos contando los segundos que van pasando y ademas agregamos 
                if (segundo == resta):
                    print("1 s más")
                    resta = resta + 1
                    flag = True
                #en caso de desincronizacion, arreglamos el contador
                else:
                    resta = segundo+1
                fps = 1/(cTime - pTime)
                fps_avg = (fps_avg+fps)/2
    
                #procesamos los frames para adaptarlos a diferentes condiciones
                cImg = cv.cvtColor(cImg,cv.COLOR_BGR2GRAY)
                ROI_nariz = cv.cvtColor(ROI_nariz,cv.COLOR_BGR2GRAY)
                ROI_nariz = cv.normalize(ROI_nariz, cImg, 0, 255, cv.NORM_MINMAX)
                ROI_nariz = cv.equalizeHist(ROI_nariz)
                
                ROI_frente = cv.cvtColor(ROI_frente,cv.COLOR_BGR2GRAY)
                ROI_frente = cv.normalize(ROI_frente, cImg, 0, 255, cv.NORM_MINMAX)
                ROI_frente = cv.equalizeHist(ROI_frente)
                
                #buscamos los puntos que son de interés
                corners_nariz = cv.goodFeaturesToTrack(ROI_nariz,mask=None,**gftt_params)
                corners_frente = cv.goodFeaturesToTrack(ROI_frente,mask=None,**gftt_params)
    
    
                #dibujamos los puntos de interes
                for i in corners_nariz:
                    x = int(i[0][0])
                    y = int(i[0][1])
                    cv.circle(ROI_nariz,(x,y),2,(255,0,0),-1)
                
                for i in corners_frente:
                    x = int(i[0][0])
                    y = int(i[0][1])
                    cv.circle(ROI_frente,(x,y),2,(255,0,0),-1)
                
                #trackeamos los puntos que anteriormente obtuvimos, asi encontraremos una diferencia,
                #de poscion en cada cuadro
                flow_points_nariz,s,e = cv.calcOpticalFlowPyrLK(pROI_nariz,ROI_nariz,corners_nariz,nextPts_nariz,**lk_params)
                flow_points_frente,s,e = cv.calcOpticalFlowPyrLK(pROI_frente,ROI_frente,corners_frente,nextPts_frente,**lk_params)
                

                #nos quedamos siempre con 30 puntos, esto para evitar errores en el dataframe
                y_tmp = []
                if len(flow_points_nariz) >= 30:
                    c+=1
                    for i in range(30):
                        y_tmp.append(flow_points_nariz[i][0][1])
                    
                    df_tmp = pd.DataFrame([y_tmp])
                    df_nariz = df_nariz.append(df_tmp,ignore_index=True)
    
                y_tmp = []
                if len(flow_points_frente) >= 30:
                    c+=1
                    for i in range(30):
                        y_tmp.append(flow_points_frente[i][0][1])
                 
                    df_tmp = pd.DataFrame([y_tmp])
                    df_frente = df_frente.append(df_tmp,ignore_index=True)
                
                #seteamos las variables siguiente, para la proxima iteración
                corners_nariz = nextPts_nariz
                corners_frente = nextPts_frente
                pROI_nariz = ROI_nariz.copy()
                pROI_frente = ROI_frente.copy()
    
                pTime = cTime
                
                #imprimimos fps por pantalla
                cImg = cv.cvtColor(cImg,cv.COLOR_GRAY2BGR)
                cv.putText(cImg,"FPS: "+str(int(fps)),(70,50),cv.FONT_HERSHEY_SIMPLEX,1.3,(0,255,0),1)
                cv.imshow("Imagen original",cImg)
                cv.imshow("Nariz",ROI_nariz)
                cv.imshow("Frente",ROI_frente)

            #cada 10 segundos realizamos una nueva medicion
            if(resta%10 == 9 and flag):
                try:
                    print("measuring...")
                    flag=False
                    
                    #--------------------------nariz--------------------------#
                    
                    maxs=[]
                    # Los datos de la nariz guardados en el dataframe df_nariz
                    #estos datos son pasados por un filtro butter pasabandas, de 5to orden
                    #ademas se filtra entre los 0.75Hz y 2.5Hz, ya que es el rango de 
                    #latencia cardiaca util para esta investigacion.
                    y_bandpass = butter_bandpass_filter(df_nariz, 0.75, 2.5, int(fps_avg), order=5)
                    df_bandpass = pd.DataFrame(y_bandpass)   
        
                    # Estandarización de datos nariz
                    df_std = StandardScaler().fit_transform(df_bandpass)
                    #reducimos la dimensionalidad, quedandonos con 5 columnas mas representativas
                    df_pca = pd.DataFrame(do_pca(df_std))
                    #calculamos mediante la funcion welch, la PSD. Esto nos entrega cual de las 5 componentes,
                    #tiene mayor periodicidad, la que tenga mayor será elegida para calculos.
                    for h in range(5):
                        f, Pxx_den = welch(df_pca[h], int(fps_avg),nperseg=int(fps_avg), noverlap=int(fps_avg)-1)
                        maxs.append(max(Pxx_den))

                    #cada vez que realizamos una medicion, se limpian los datos, para evitar
                    #algun tipo de sobreajuste
                    del df_nariz
                    df_nariz=pd.DataFrame(columns= [0,1,2,3,4,5,6,7,8,9,10,11,12])
                    
#-----------------------------------frente-----------------------------------------------
                    #se repite lo mencionado anteriormente mencionado pero ahora para el dataframe de la frente
                    #luego juntaremos todo y analizaremos                    
                    y_bandpass = butter_bandpass_filter(df_frente, 0.75, 2.5, int(fps_avg), order=5)
                    df_bandpass = pd.DataFrame(y_bandpass)   
                    
                    # Estandarización de datos frente
                    df_std = StandardScaler().fit_transform(df_bandpass)
                    df_pca = pd.DataFrame(do_pca(df_std))
                    for h in range(5):
                        f, Pxx_den = welch(df_pca[h], int(fps_avg),nperseg=int(fps_avg), noverlap=int(fps_avg)-1)
                        maxs.append(max(Pxx_den))
                    
                    del df_frente
                    df_frente=pd.DataFrame(columns= [0,1,2,3,4,5,6,7,8,9,10,11,12])
                    
                    
                    #finalmente seleccionamos la curva más periodica, y segun la investigaciones,
                    #al multiplicar su periodicidad por 60, encontramos el heart rate final 
                    print("heart rate:",int(max(maxs)*60))
                    
                    #control de suma de heart rate promedio
                    if is_first:
                        avg_hr = int(max(maxs)*60)
                        is_first = False
                    else:
                        avg_hr = (avg_hr+int(max(maxs)*60))/2
                    
                   
                    
                    
                except Exception as err:
                    print(err)
                    pass
                
        except Exception as err:
            print(err)
            pass


    print("heart rate promedio:",int(avg_hr))
    cap.release()
    cv.destroyAllWindows()
    
    

if __name__ == '__main__':
    main() 
    
####################### ANALISIS DE GRAFICO######################################
#obsoleto, pero puede servir de ayuda para otros propositos

#     stop_time = time.time()
#     y_bandpass = butter_bandpass_filter(df[30:], 0.75, 2.5, int(fps_avg), order=5)
#     df_bandpass = pd.DataFrame(y_bandpass)   

#     # Standardizing the features
#     df_std = StandardScaler().fit_transform(df_bandpass)
#     df_pca = pd.DataFrame(do_pca(df_std))
    
# ##-----------------------Plots-----------------------#
#     time = np.linspace(0,round(stop_time-initial_time),c//2)
#     time = time[30:]
#     # plt.plot(time[30:],y_axis[30:])
#     # plt.title("time/Y")
#     # plt.show()
    
#     maxs=[]
#     print("promedio fps",int(fps_avg))
#     for i in range(5):
        
#         # plt.plot(time,df_bandpass[i])
#         # plt.title("Bandpass "+str(i))
#         # plt.show()
        
#         plt.plot(time,df_pca[i])
#         plt.title("PCA "+str(i))
#         plt.show()
        
        
#         df_fft = rfft(df_pca[i].tolist())
#         # df_fft = butter_bandpass_filter(df_fft, 0.75, 2.5, 30)

#         xf = rfftfreq(len(df_pca[i]),int(fps_avg))
#         plt.plot(xf,np.abs(df_fft))
#         # plt.xlim([0,3])
#         plt.title("FFT "+str(i))
#         plt.show()
        
#         # plt.psd(df_pca[i].tolist(),int(fps_avg),4,Fc=0.7)
#         # plt.title("psd "+str(i))
#         # plt.show()
        
        
#         # f,psd = numpy_psd(df_pca[i],int(fps_avg))
#         # plt.plot(f,psd)
#         # plt.show()
        
#         f, Pxx_den = welch(df_pca[i], int(fps_avg), nperseg=int(fps_avg))
#         plt.plot(f,Pxx_den)
#         plt.show()
    
        
        
# #     print(max(maxs)*60)
