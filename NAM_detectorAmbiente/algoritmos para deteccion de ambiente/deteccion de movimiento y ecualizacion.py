import cv2
import numpy as np
import argparse

## Opciones para ejecucion de programa
parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file',
    help='Path to video file (if not using camera)')
parser.add_argument('-b', '--bins', type=int, default=256,
    help='Number of bins per channel (default 256)')

parser.add_argument('-t', '--threshold', type=int, default=2000,
    help='Number of threshold (default 2000)')

args = vars(parser.parse_args())


if not args.get('file', False):
    capture = cv2.VideoCapture(0)
else:
    capture = cv2.VideoCapture(args['file'])

bins = args['bins']

## Se define función para graficar histograma 
height = 128
def plot_hist(hist, color):
    hist = hist.astype(int)
    img = np.zeros((height, bins))
    for i in range(bins):
        cv2.line(img, (i, height), (i, height - hist[i]), color)
    return img

## Se define umbral para el movimiento
threshold = args['threshold']

## Inicializa variable previous
previous = np.zeros((256, 1), np.float32)

while True:
    grabbed, frame = capture.read()

    if not grabbed:
        break

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    hist = cv2.calcHist([gray], [0], None, [bins], [0, 256])

    ## Ecualización de histograma
    eqgray = cv2.equalizeHist(gray)
    eqgrayhist = cv2.calcHist([eqgray], [0], None, [bins], [0, 256])

    
    diff = hist - previous 

    ## Normalizacion de histogramas
    norm_hist = hist * (height / np.max(hist))
    norm_diff = diff * (height / np.max(hist))
    norm_hist_gray = eqgrayhist * (height / np.max(hist))
    
    ## Grafica histogramas
    gray[      :  height, :bins] = plot_hist(norm_hist.ravel(), 127)
    gray[height:2*height, :256] = plot_hist(norm_diff.ravel(), 127)
    eqgray[      :  height, :bins] = plot_hist(norm_hist_gray.ravel(), 127)

    ## Comparación de histogramas
    dist = cv2.compareHist(hist, previous, cv2.HISTCMP_CHISQR)
    previous = hist.copy()
    
    ## Mostrar texto en pantalla para dar aviso de movimiento
    cv2.putText(gray, '%.2f' % dist, (0, 3*height//2), fontScale=2,
                fontFace=cv2.FONT_HERSHEY_SIMPLEX, thickness=2,
                color=255, lineType=cv2.LINE_AA)
    if dist > threshold:
        cv2.putText(gray, 'Movimiento Detectado', (50, 450), fontScale=1,
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, thickness=2,
                    color=255, lineType=cv2.LINE_AA)

    ## Mostrar salida de deteccion de movimiento y ecualización
    cv2.imshow('frame', gray)
    cv2.imshow('eqgray',eqgray)
    if cv2.waitKey(20) & 0xFF == 27:
        break

capture.release()
cv2.destroyAllWindows()